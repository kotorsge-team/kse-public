# KSE Public Repository
This is a public repository that'll hold test and preview build of KSE/KPF.

To download them, please visit the [downloads](https://bitbucket.org/kotorsge-team/kse-public/downloads/) section.
